# HalloweenThemeViolentMonkey

A non-perfect Halloween Theme for Violent Monkey! Native IDE is broken (text selection is not visible.) I suggest using another IDE instead of the Violent Monkey IDE to circumvent this issue. 

# Instructions:

1. Visit and copy the contents of: https://gitlab.com/c9x3/halloweenthemeviolentmonkey/-/raw/main/HalloweenThemeViolentMonkey.css.

2. Within Violent Monkeys extension option page, go to Settings -> Advanced -> Custom Style and paste all of the contents of, "HalloweenThemeViolentMonkey.css" into the custom css box and click save.

# Licensing and donation information:

https://c9x3.neocities.org/

# Original license: 

The MIT License (MIT)

Copyright (c) 2018 Dracula Theme

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

https://github.com/gkroon/dracula-css

